import random

class UI:
    def __init__(self, name):
        self.name = name
        self.first_run = True
        self.say("HI!")
        self.say("I'm a helpful chatbot")
        self.say("Ask me something!")

    def say(self, text):
        print(f"{self.name} >> {text}")

    def ask(self, text):
        return input(f"{self.name} >> {text}")

    def get_command(self):
        prompts = [
            "How can I help you? ",
            "Let me know if how can I help you? ",
            "Is there anything else I can help you with? ",
            "How else can I help you with? ",
        ]

        if self.first_run:
            prompt = prompts[0]
        else:
            prompt = random.choice(prompts)

        self.first_run = False
        command = self.ask(prompt)
        split = command.strip().split()
        command = split[0].lower()
        args = split[1:]
        return [command, args]
        

