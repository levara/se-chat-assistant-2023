import random

class JokeHandler:
    def __init__(self, ui, args):
        self.ui = ui
        self.args = args

    def handle(self):
        jokes = [
            ["Why did the chicken cross the road?",
             "To get to the other side!"],
            ["I went to find some camo pants, but couldn't find any..."],
            ["I used to have a handle on life, but then it broke"],
            ["I want to die peacefully in my sleep, like my grandfather...",
             "Not screaming and yelling like the passengers in his car."]
        ]

        joke = random.choice(jokes)

        self.ui.say("Here a joke: ")
        for line in joke:
            self.ui.say(line)
        self.ui.say("HAHAHAH")
