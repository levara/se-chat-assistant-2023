import random
import requests

class CurrencyConverterHandler:
    def __init__(self, ui, args):
        self.ui = ui
        self.args = args

    def args_valid(self):
        if self.currency_from not in self.available_currencies:
            return False
        if self.currency_to not in self.available_currencies:
            return False
        try:
            self.amount = float(self.amount)

        except Exception as e:
            return False
        return True

    def handle(self):
        self.get_available_currencies()
        if len(self.args) == 3:
            self.currency_from = self.args[0]
            self.currency_to = self.args[1]
            self.amount = self.args[2]
        else:
            self.ui.say("Can't understand the parameters.")
            return False

        if self.args_valid():
            self.ui.say("Parametri ispravni")
        else:
            self.ui.say("Parametri neispravni")
            return False

        converted = self.convert()

        self.ui.say(f"{self.amount} {self.currency_from} jednako je {converted} {self.currency_to}")
            

    def get_available_currencies(self):
        url = "https://currency-exchange.p.rapidapi.com/listquotes"

        headers = {
            "X-RapidAPI-Key": "0a79803ed3mshc384cde749c0e05p119b5ajsnc9242f1839cf",
            "X-RapidAPI-Host": "currency-exchange.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers)

        self.available_currencies = response.json()
        print(self.available_currencies)

    def convert(self):
        url = "https://currency-exchange.p.rapidapi.com/exchange"

        querystring = {
                "to": self.currency_to,
                "from": self.currency_from,
                "q": "1"
        }

        headers = {
            "X-RapidAPI-Key": "0a79803ed3mshc384cde749c0e05p119b5ajsnc9242f1839cf",
            "X-RapidAPI-Host": "currency-exchange.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers, params=querystring)

        self.exchange_rate = float(response.json())

        return self.amount * self.exchange_rate


if __name__ == "__main__":
    from ui import UI
    ui = UI("Billy")
    handler = CurrencyConverterHandler(ui, [100])
    handler.handle()
