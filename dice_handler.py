import random

class DiceHandler:
    def __init__(self, ui, args):
        self.ui = ui
        self.args = args

    def handle(self):
        num_dices = 1
        num_sides = 6
        if len(self.args) == 1:
            num_dices = int(self.args[0])
        elif len(self.args) == 2:
            num_dices = int(self.args[0])
            num_sides = int(self.args[1])
        for _ in range(num_dices):
            random_number = random.randint(1,num_sides)
            self.ui.say(f"Your {num_sides}-sided dice roll is: {random_number}")
