
import os, sys
import random

import requests
from ui import UI

from dice_handler import DiceHandler
from joke_handler import JokeHandler
from currency_converter_handler import CurrencyConverterHandler

ui = UI("Billy")

while True:
    command,args = ui.get_command()

    if command == "bye":
        ui.say("Byeee")
        sys.exit()

    elif command == "dice":
        handler = DiceHandler(ui, args)
        handler.handle()

    elif command == "joke":
        handler = JokeHandler(ui, args)
        handler.handle()

    elif command == "convert":
        handler = CurrencyConverterHandler(ui, args)
        handler.handle()

    else: 
        print("Unrecognized command")


    
